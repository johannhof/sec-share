Sec-Share
=========

A secure, Dropbox-like application written in Java for the Security course at FCUL.

It was originally written in Java 8, which I had to throw out because the lab computers don't support it.